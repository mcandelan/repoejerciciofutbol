package com.ejerciciofutbol;

import java.util.ArrayList;

public class Jugador {

	private String nombre;
	private int dni;
	private float salario;
	private Auto auto;
	private Club club;
	private ArrayList<Auto> autos;
	
	
	public Club getClub() {
		return club;
	}

	public void setClub(Club club) {
		this.club = club;
	}

	public Jugador(String nombre, int dni, float salario) {
		super();
		this.nombre = nombre;
		this.dni = dni;
		this.salario = salario;
		this.autos = new ArrayList<Auto>();
	}
	
	public Jugador() {
		
		this.autos = new ArrayList<Auto>();
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public float getSalario() {
		return salario;
	}

	public void setSalario(float salario) {
		this.salario = salario;
	}
	
	
	public void meterGol(int cant) {
		
		for(int i=0;i<cant;i++) {
		this.salario = this.salario + ((this.salario*5)/100);
		}
	}
	
	public void tenerRoja(int cant) {
		
		for(int i=0;i<cant;i++) {
		
		this.salario= this.salario - ((this.salario*5)/100);
	}
		}
	
	public void agregarAuto(Auto a) {
		
		this.autos.add(a);
		
	}
	
	public void listarAutos() {
		
		System.out.println("\n");
		
		System.out.println("Listado de autos del jugador "+this.nombre);
		for (Auto aut: this.autos ) {
			
			System.out.println("Patente "+aut.getPatente());
			System.out.println("Color "+aut.getColor());
		}
	}
	
	
	
	
}

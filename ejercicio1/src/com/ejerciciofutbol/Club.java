package com.ejerciciofutbol;

import java.util.ArrayList;

public class Club {
	
	private String nombre;
	private String barrio;
	private Jugador jugador;
	private ArrayList<Jugador> jugadores;
	
	public Club(String nombre, String barrio) {
		super();
		this.nombre = nombre;
		this.barrio = barrio;
		this.jugadores = new ArrayList<Jugador>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}
	
	public void agregarJugador(Jugador jug) {
		
		this.jugadores.add(jug);
		jug.setClub(this);
	}
	
	public void listarJugadores() {
		
		System.out.println("\n");
		System.out.println("Listado de jugadores del club " + this.nombre+":");
		for (Jugador ju: this.jugadores ) {
			
			System.out.println("Jugador "+ju.getNombre());
			System.out.println("DNI:  "+ju.getDni());
		}
	}
	
	

}

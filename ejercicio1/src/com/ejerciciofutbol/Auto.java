package com.ejerciciofutbol;

public class Auto {

	
	private String patente;
	private String color;
	
	
	public Auto(String patente, String color) {
		super();
		this.patente = patente;
		this.color = color;
	}
	
	public Auto(){
		
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
	
	
	
}

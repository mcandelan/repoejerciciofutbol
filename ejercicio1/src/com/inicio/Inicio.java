package com.inicio;

import com.ejerciciofutbol.Auto;
import com.ejerciciofutbol.Club;
import com.ejerciciofutbol.Jugador;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		Jugador jugador1= new Jugador("Carlos Tevez",123,1500);
		Jugador jugador2= new Jugador("Riquelme", 4456, 2500);
		Jugador jugador3= new Jugador("Mascherano",886,15000);
		
		Club club1= new Club("River", "Nu�ez");
		Club club2= new Club ("San Carlos", "San Isidro");
		
		Auto aut1= new Auto("FGC-445", "Verde");
		Auto aut2= new Auto("JKL-225", "Rosa");
		Auto aut3= new Auto("VVN-457", "Violeta");
		Auto aut4= new Auto("DKL-521", "Gris");
	
		
		int roja=3;
		int gol=5;
		System.out.println("El salario actual de "+jugador1.getNombre()+ " es: "+jugador1.getSalario());
		jugador1.tenerRoja(roja);
		System.out.println("Como tuvo "+roja+"  tarjetas rojas, ahora su salario es: "+jugador1.getSalario());
		
		System.out.println("El salario actual de "+jugador2.getNombre()+ " es: "+jugador2.getSalario());
		jugador2.meterGol(gol);
		System.out.println("Como meti� "+gol+" goles, ahora su salario es: "+jugador2.getSalario());
		
		jugador1.agregarAuto(aut1);
		jugador1.agregarAuto(aut2);
		jugador2.agregarAuto(aut3);
		jugador2.agregarAuto(aut4);
		
		club1.agregarJugador(jugador1);
		club1.agregarJugador(jugador2);
		club2.agregarJugador(jugador3);
		
		
		
		club1.listarJugadores();
		club2.listarJugadores();
		
		jugador1.listarAutos();
		jugador2.listarAutos();
		
		System.out.println("jugador " + jugador1.getNombre()+" pertenece al club "+jugador1.getClub().getNombre());
		

	}

}
